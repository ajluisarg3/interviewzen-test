import java.io.IOException;
import java.util.Date;
import java.util.List;

import beans.Drink;
import beans.Food;
import beans.Menu;
import beans.Offer;
import beans.Order;
import beans.Product;
import dao.OfferDao;
import dao.OrderDao;
import dao.ProductDao;
import exceptions.BusinessException;

public class RestaurantManagement {

	/**
	 * Create an order
	 * @param foods
	 * @param drinks
	 * @param menus
	 * @throws IOException
	 * @throws BusinessException
	 */
	public static void orderMeal(List<Food> foods, List<Drink> drinks, List<Menu> menus) throws IOException, BusinessException {
		Order order = new Order(foods, drinks, menus);
		order.initTheOrder();
		order.generateBill();
	}
	
	/**
	 * Get economic summary by range date
	 * @param initDate
	 * @param endDate
	 */
	public static void economicSummary(Date initDate, Date endDate) {
		//this method get the economicSummary of a data range
		//initDate and endDate are params that will be use when find in a database
		double total = 0;
		for(Order order: OrderDao.find()) {
			System.out.println(order.getOrderDate() + " Income: "+order.getPrice());
			total+=order.getPrice();
		}
		System.out.println("Total: "+total);
	}
	
	/**
	 * Refill Stock of product
	 * @param productId
	 * @param quantity
	 */
	public static void refillStock(String productId, int quantity) {
		Product product = ProductDao.findById(productId);
		product.setStock(product.getStock()+quantity);
		ProductDao.update(productId, product);
		//Also for save up one database operation (read and write) we can execute a findAndUpdate operation
	}
	/**
	 * Create an offer
	 * @param offer
	 */
	public static void createOffer(Offer offer) {
		OfferDao.save(offer);
	}
	
	/**
	 * Show offer list
	 */
	public static void showOffers() {
		List<Offer> offers = OfferDao.find();
		// Process offers data
	}
	
}
