package beans;

import java.util.List;
import java.util.UUID;

import beans.enums.OfferType;

public class Offer {
	private String id;
	private String name;
	private double price;
	private List<Food> foods;
	private List<Drink> drinks;
	private OfferType type;
	
	
	public Offer(String name, double price, List<Food> foods, List<Drink> drinks, OfferType type) {
		super();
		this.id = UUID.randomUUID().toString();
		this.name = name;
		this.price = price;
		this.foods = foods;
		this.drinks = drinks;
		this.type = type;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public List<Food> getFoods() {
		return foods;
	}
	public void setFoods(List<Food> foods) {
		this.foods = foods;
	}
	public List<Drink> getDrinks() {
		return drinks;
	}
	public void setDrinks(List<Drink> drinks) {
		this.drinks = drinks;
	}
	public OfferType getType() {
		return type;
	}
	public void setType(OfferType type) {
		this.type = type;
	}
}
