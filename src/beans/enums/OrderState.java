package beans.enums;


public enum OrderState {
	PREPARING,
	SENT,
	DELIVERED
}
