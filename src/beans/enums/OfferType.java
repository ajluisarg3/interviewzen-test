package beans.enums;

public enum OfferType {
    DAILY_MENU,
    WEEKEND_MENU,
    HAPPY_HOUR
}
