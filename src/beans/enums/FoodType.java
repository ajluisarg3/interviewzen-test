package beans.enums;


public enum FoodType {
	MEAT,
	FISH,
	VEGETABLE
}
