package beans;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import beans.enums.OrderState;
import exceptions.BusinessException;

import java.util.logging.Level;
import java.util.logging.Logger;



public class Order {

	private final static Logger LOGGER = Logger.getLogger(Order.class.getName());
	
	private String id;
	private List<Food> foods;
	private List<Drink> drinks;
	private List<Menu> menus;
	private boolean payed;
	private OrderState state;
	private double price;
	private Date orderDate;

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Order(List<Food> foods, List<Drink> drinks, List<Menu> menus) {
		super();
		this.id = UUID.randomUUID().toString();
		this.foods = foods;
		this.drinks = drinks;
		this.menus = menus;
		this.price = calculateOrderPrice();
		this.orderDate = new Date();
	}

	public void initTheOrder() throws BusinessException {
		this.payed = true;
		this.state = OrderState.PREPARING;
		// Update products stock
		List<Product> products = new ArrayList<>();
		if (this.foods != null && !this.foods.isEmpty())
			products.addAll(this.foods);
		if (this.drinks != null && !this.drinks.isEmpty())
			products.addAll(this.drinks);
		
		for(Product product : products) {
			if (product.getStock() == 0) {
				sendStockNotification();
				throw new BusinessException(product.getName() + " have't stock");
			}
			product.setStock(product.getStock() - 1);
		}
	}

	private void sendStockNotification() {
		// Send a notification to refill stock
		
	}

	private double calculateOrderPrice() {

		double totalPrice = 0;

		List<Product> products = new ArrayList<>();
		if (this.foods != null && !this.foods.isEmpty())
			products.addAll(this.foods);
		if (this.drinks != null && !this.drinks.isEmpty())
			products.addAll(this.drinks);

		if (this.menus != null && !this.menus.isEmpty())
			totalPrice += this.menus.stream().mapToDouble(i -> i.getPrice() + i.getTaxes()).sum();

		totalPrice += products.stream().mapToDouble(i -> i.getPrice() + i.getTaxes()).sum();

		return new BigDecimal(totalPrice).setScale(2, RoundingMode.HALF_UP).doubleValue();

	}

	public void generateBill() throws IOException {

		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter("Bill " + this.id + ".txt"));
			writer.write("Bill " + this.id + this.orderDate);
			writer.newLine();
			writer.newLine();
			writer.write("--- Foods ---");
			writer.newLine();
			this.foods.forEach(food -> {

				try {
					writer.write(food.getName() + ": " + food.getPrice() + " | Taxes: " + food.getTaxes());
					writer.newLine();

				} catch (IOException e) {
					e.printStackTrace();
				}

			});
			writer.newLine();
			writer.write("--- Drinks ---");
			writer.newLine();
			this.drinks.forEach(drink -> {

				try {
					writer.write(drink.getName() + ": " + drink.getPrice() + " | Taxes: " + drink.getTaxes());
					writer.newLine();

				} catch (IOException e) {
					//Java logger example
					LOGGER.log(Level.WARNING, "Error saving bill "+e);
				}

			});

			writer.newLine();
			writer.write("--- Menus ---");
			writer.newLine();
			this.menus.forEach(menu -> {

				try {
					writer.write(menu.getName() + ": " + menu.getPrice() + " | Taxes: " + menu.getTaxes());
					writer.newLine();

				} catch (IOException e) {
					e.printStackTrace();
				}

			});

			writer.newLine();
			writer.write("Total: " + this.price);

			writer.flush();
			writer.close();
		} catch (IOException ex) {
			System.out.println("File could not be created");
		}
	}
	
	// The file will be something like that
	/*
	 
	 Bill a7c53572-37c8-48bf-8970-7114b5403eb3Wed Jun 27 15:56:33 CEST 2018

		--- Foods ---
		Burger: 13.2 | Taxes: 2.0
		Apple: 14.2 | Taxes: 2.0
		
		--- Drinks ---
		Water: 14.2 | Taxes: 1.0
		
		--- Menus ---
		Menú mediano: 11.0 | Taxes: 1.0
		
		Total: 58.6
	 
	 */

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getId() {
		return id;
	}

	public OrderState getState() {
		return state;
	}

	public void setState(OrderState state) {
		this.state = state;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Food> getFoods() {
		return foods;
	}

	public void setFoods(List<Food> foods) {
		this.foods = foods;
	}

	public List<Drink> getDrinks() {
		return drinks;
	}

	public void setDrinks(List<Drink> drinks) {
		this.drinks = drinks;
	}

	public List<Menu> getMenus() {
		return menus;
	}

	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

	public boolean isPayed() {
		return payed;
	}

	public void setPayed(boolean payed) {
		this.payed = payed;
	}

	public void sendTheOrder() {
		// Business logic to send the order
	}
}
