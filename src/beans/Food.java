package beans;

import beans.enums.FoodType;

public class Food extends Product {


	private FoodType type;
	private boolean suitableForCoeliacs;
	
	public Food(String name, double price, double taxes, boolean available, FoodType type, boolean suitableForCoeliacs, int stock) {
		super(name, price, taxes, available, stock);
		this.type = type;
		this.suitableForCoeliacs = suitableForCoeliacs;
	}
	
	public FoodType getType() {
		return type;
	}
	public void setType(FoodType type) {
		this.type = type;
	}
	public boolean isSuitableForCoeliacs() {
		return suitableForCoeliacs;
	}
	public void setSuitableForCoeliacs(boolean suitableForCoeliacs) {
		this.suitableForCoeliacs = suitableForCoeliacs;
	}

}
