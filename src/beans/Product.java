package beans;

import java.util.UUID;

public class Product {

	private String name;
	private String id;
	private double price;
	private double taxes;
	private boolean available;
	private int stock;
	private double cost;

	
	// In a real implementation we can use lombok and @Data annotation
	
	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
		if (stock == 0)
			available = false;
	}

	public void setTaxes(double taxes) {
		this.taxes = taxes;
	}

	public Product(String name, double price, double taxes, boolean available, int stock) {
		super();
		this.name = name;
		this.id = UUID.randomUUID().toString();
		this.price = price;
		this.taxes = taxes;
		this.available = available;
		this.stock = stock;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getTaxes() {
		return taxes;
	}

	public void setTaxes(int taxes) {
		this.taxes = taxes;
	}

}
