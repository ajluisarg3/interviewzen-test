package beans;

public class Drink extends Product {

	private int size;
	private boolean haveAlcohol;
	
	
	public Drink(String name, double price, double taxes, boolean available, int size, boolean haveAlcohol, int stock) {
		super(name, price, taxes, available, stock);
		this.size = size;
		this.haveAlcohol = haveAlcohol;
	}



	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public boolean isHaveAlcohol() {
		return haveAlcohol;
	}

	public void setAlcohol(boolean haveAlcohol) {
		this.haveAlcohol = haveAlcohol;
	}

}
