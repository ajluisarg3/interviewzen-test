package beans;

import java.util.List;
import java.util.UUID;

public class Menu {

	private String id;
	private String name;
	private List<Food> foods;
	private List<Drink> drinks;
	private double price;
	private double taxes;
	
	public double getTaxes() {
		return taxes;
	}
	public void setTaxes(double taxes) {
		this.taxes = taxes;
	}
	public Menu(String name, List<Food> foods, List<Drink> drinks, double price, double taxes) {
		this.id = UUID.randomUUID().toString();
		this.name = name;
		this.foods = foods;
		this.drinks = drinks;
		this.price = price;
		this.taxes = taxes;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public List<Food> getFoods() {
		return foods;
	}
	public void setFoods(List<Food> foods) {
		this.foods = foods;
	}
	public List<Drink> getDrinks() {
		return drinks;
	}
	public void setDrinks(List<Drink> drinks) {
		this.drinks = drinks;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}


}
