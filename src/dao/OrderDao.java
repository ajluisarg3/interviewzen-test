package dao;

import java.util.ArrayList;
import java.util.List;

import beans.Drink;
import beans.Food;
import beans.Menu;
import beans.Order;
import beans.enums.FoodType;

// This is a simple class that emulate acces to database
public class OrderDao {
	public static List<Order> find() {
		
		Food burger = new Food("Burger", 13.2, 2, true, FoodType.MEAT, false, 1);
		Food apple = new Food("Apple", 14.2, 2, true, FoodType.VEGETABLE, false, 1);
		Drink water = new Drink("Water", 14.2, 1, true, 200, false, 1);
				
		List<Food> foods = new ArrayList<>();
		foods.add(burger);
		foods.add(apple);
		List<Drink> drinks = new ArrayList<>();
		drinks.add(water);
		
		Menu menu = new Menu("Menú mediano", foods, drinks, 11, 1);

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);
		
		Order order = new Order(foods, drinks, menus);
		
		List<Order> orders = new ArrayList<>();
		orders.add(order);
		orders.add(order);
		
		return orders;
	}
	
}
