package dao;

import java.util.ArrayList;
import java.util.List;

import beans.Drink;
import beans.Food;
import beans.Offer;
import beans.enums.FoodType;
import beans.enums.OfferType;

// This is a simple class that emulate acces to database
public class OfferDao {
	public static void save(Offer offer) {
		System.out.println("Saving offer...");
	}
	
	public static List<Offer> find() {
		Food burger = new Food("Burger", 13.2, 2, true, FoodType.MEAT, false, 1);
		Food apple = new Food("Apple", 14.2, 2, true, FoodType.VEGETABLE, false, 1);
		Drink water = new Drink("Water", 14.2, 1, true, 200, false, 1);
				
		List<Food> foods = new ArrayList<>();
		foods.add(burger);
		foods.add(apple);
		List<Drink> drinks = new ArrayList<>();
		drinks.add(water);
		
		Offer offer = new Offer("Oferta de verano", 10, foods, drinks, OfferType.WEEKEND_MENU);
		List<Offer> offers = new ArrayList<>();
		offers.add(offer);
		
		return offers;
	}
	
}
