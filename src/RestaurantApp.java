import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import beans.Drink;
import beans.Food;
import beans.Menu;
import beans.enums.FoodType;
import exceptions.BusinessException;


//Simple class with a main method to test exercise
//All data is fake, In a real application the products are stored in a database and we should finding by id
public class RestaurantApp {

	
	// Exercise 1
	
	/*
	 
	public static void main(String[] args) {
		
		String exampleData = "A56B455VB23GTY23J";
		
		System.out.println(processData(exampleData));
		// Print [23, 56, 455]
	}

	 * This method has several stages:
	 * 0 - Convert the array to Stream (for Java 8 lambdas processing)
	 * 1 - Split the String to get only numbers (Regex)
	 * 2 - Filter to delete empty elements
	 * 3 - Map to elemets to int
	 * 4 - Sort the elements ascending
	 * 5 - Collect the elements to a list of integers
	 * @param exampleData
	 * @return
	
	private static List<Integer> processData(String exampleData) {
		
		String[] processedData = exampleData.split("[A-Z]");
		
		return Arrays.stream(processedData)
		        .filter(element -> !element.isEmpty())
		            .mapToInt(Integer::parseInt).distinct()
		                .sorted().boxed().collect(Collectors.toList());
		        
		
	}
	 
	 */
	
	
	public static void main(String[] args) throws IOException, BusinessException {
		
		// Here the products are created (mock data)
		
		Food burger = new Food("Burger", 13.2, 2, true, FoodType.MEAT, false, 1);
		Food apple = new Food("Apple", 14.2, 2, true, FoodType.VEGETABLE, false, 1);
		Drink water = new Drink("Water", 14.2, 1, true, 200, false, 1);
		
		
		List<Food> foods = new ArrayList<>();
		foods.add(burger);
		foods.add(apple);
		List<Drink> drinks = new ArrayList<>();
		drinks.add(water);
		
		Menu menu = new Menu("Big menu", foods, drinks, 11, 1);

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);
		
		RestaurantManagement.orderMeal(foods, drinks, menus);
		
		RestaurantManagement.economicSummary(null, null);
		
		RestaurantManagement.showOffers();
	}

}
